#!/bin/bash

# Get Current Path
TOP_PATH=$(dirname "$0")

# Check if python 3 is installed
PY3_INSTALLED=0
command -v python3 > /dev/null 2>&1 && PY3_INSTALLED=1
if [[ "$PY3_INSTALLED" -eq 0 ]]; then
    echo ":( Oops! Did not find python3"
    exit 1
else 
    echo ":) Good! Found python3"
fi

# Setup virtual environment if not found
PI_VENV3_PATH="$HOME/.pi_venv3"
if [ -d "$PI_VENV3_PATH" ]; then
    echo ":) Good! Python virtual environment exists, skipping creation"
else
    echo ":D Hmmm! Creating python virtual environment at : $PI_VENV3_PATH"
    python3 -m venv $PI_VENV3_PATH
fi

# Activate virtual environment
source $PI_VENV3_PATH/bin/activate

# Install/Update requirements 
pip3 install -r "$TOP_PATH/requirements.txt"

# Start the server
python "$TOP_PATH/src/app.py"

# Deactivate virtual environment
deactivate